const Q = require('q');
const jwt = require('jsonwebtoken');
const config = require('../../../config');
const checkRequestBody = require('../../utils/request.body.verifier');


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'username',
        'password'
    ]);
    if (error) {
        defer.reject({
            code: 400,
            msg: error
        });
    }
    defer.resolve(true);
    return defer.promise;
};


const verifyPassword = async(username, password) => {
    const defer = Q.defer();
    try {
        if (username === 'admin' && password === 'admin') {
            const token = jwt.sign({ username }, config.secretKey);
            const data = {
                token
            };
            defer.resolve(data);
        } else {
            defer.reject({
                code: 401,
                msg: 'Invalid Username/Password'
            });
        }
    } catch (e) {
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


async function loginUser(req, res) {
    try {
        const { body } = req;
        const { username, password } = body;
        await checkRequest(body);
        const response = await verifyPassword(username, password);
        res.status(200).json({
            message: 'User Logged in',
            login_data: response
        });
    } catch (e) {
        res.status(e.code).json({
            message: e.msg,
            error: 'Bad Request'
        });
    }
}


module.exports = loginUser;

