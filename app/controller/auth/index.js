const loginUser = require('./login');

const authController = {
    loginUser
};

module.exports = authController;
