const Q = require('q');
const config = require('../../../config');

const checkRequestBody = require('../../utils/request.body.verifier');


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'country'
    ]);
    if (error) {
        defer.reject({
            code: 400,
            msg: error
        });
    }
    defer.resolve(true);
    return defer.promise;
};


const addCountry = (country) => {
    const defer = Q.defer();
    try {
        const { countryArray } = config;
        const filteredArr = countryArray.filter((e) => e === country);
        if (filteredArr.length > 0) {
            defer.resolve(countryArray);
        } else {
            countryArray.push(country);
            defer.resolve(countryArray);
        }
    } catch (e) {
        defer.reject({
            code: 400,
            msg: e
        });
    }
    return defer.promise;
};


async function createCountry(req, res) {
    try {
        const { body } = req;
        const { country } = body;
        await checkRequest(body);
        const response = await addCountry(country);
        res.status(200).json({
            countries_data: response
        });
    } catch (e) {
        res.status(e.code).json({
            message: e.msg,
            error: 'Bad Request'
        });
    }
}


module.exports = createCountry;

