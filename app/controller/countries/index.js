const getCountries = require('./get.countries');
const createCountry = require('./create.country');
const deleteCountry = require('./delete.country');

const countryController = {
    getCountries,
    createCountry,
    deleteCountry
};

module.exports = countryController;

