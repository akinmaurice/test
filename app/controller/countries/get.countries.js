const Q = require('q');
const config = require('../../../config');


const getCountriesData = () => {
    const defer = Q.defer();
    try {
        const { countryArray } = config;
        defer.resolve(countryArray);
    } catch (e) {
        defer.reject({
            code: 400,
            msg: e
        });
    }
    return defer.promise;
};


async function getCountries(req, res) {
    try {
        const response = await getCountriesData();
        res.status(200).json({
            countries_data: response
        });
    } catch (e) {
        res.status(e.code).json({
            message: e.msg,
            error: 'Bad Request'
        });
    }
}


module.exports = getCountries;

