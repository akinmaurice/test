const express = require('express');


const router = express.Router();
const extractUser = require('../utils/auth.controller');
const authController = require('../controller/auth');
const countryController = require('../controller/countries');


router.get(
    '/',
    (req, res) => {
        res.status(200).json({ message: 'Service' });
    },
);


router.post(
    '/login',
    authController.loginUser
);


router.get(
    '/countries',
    extractUser,
    countryController.getCountries
);


router.post(
    '/countries',
    extractUser,
    countryController.createCountry
);


router.delete(
    '/countries',
    extractUser,
    countryController.deleteCountry
);


module.exports = router;
