const jwt = require('jsonwebtoken');
const config = require('../../config/index');


const extractUser = (req, res, next) => {
    const token = req.headers.authorization;
    if (req.headers && req.headers.authorization) {
        jwt.verify(token, config.secretKey, (err, result) => {
            if (err) {
                return res.status(403).json({
                    message: 'User authentication failed'
                });
            }
            return next();
        });
    } else {
        res.status(403).json({
            message: 'User is not authorized'
        });
    }
};

module.exports = extractUser;

