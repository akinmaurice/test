# serviceApp

Simple Node JS Service API Application.

## Getting Started


Please ensure Node JS is installed.


  1.  git clone https://gitlab.com/akinmaurice/test.git
  2.  cd test
  3.  run ``` npm install```
  4.  run ``` npm start```
  5. visit localhost:3023 to view


The above will get you a copy of the project up and running on your local machine for development and testing purposes.


## API Endpoints


```
| EndPoint                                |   Functionality                      |
| --------------------------------------- | ------------------------------------:|
| POST /login                             | Login                                |
| GET /countries                          | Get Countries                        |
| POST /countries                         | Create Country                       |
| DELETE /countries                       | Delete Country                       |
```




## License

This project is licensed under the MIT License - see the [LICENSE.md](https://opensource.org/licenses/MIT) file for details
