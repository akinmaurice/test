const express = require('express');


const expressConfig = require('./config/express');

const port = process.env.PORT || 3023;
const app = express();


expressConfig(app);

app.listen(port);
logger.info(`Server started on port ${port}`);

module.exports = app;
